# Intro to DevOps Workshop

## Requirements

* [Node.js v13.4.0+](https://nodejs.org/en/download/current/)
* [Gatsby CLI v2.8.19+](https://www.gatsbyjs.org/docs/preparing-your-environment/)
* [yarn v1.21.1+](https://yarnpkg.com/lang/en/docs/install/)
* [Heroku CLI v7.35.0+](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)

On MacOS:

```
brew install node
npm install -g gatsby-cli
brew install yarn
brew tap heroku/brew && brew install heroku
```

## Installation

```
yarn install
```

## Local development

```
gatsby develop
```

## Testing

(Put in a small test to run on CI?)

## Workshop exercise

1. Deploy to Heroku manually
2. Write bash script to deploy to Heroku using CLI
3. Write Gitlab CI/CD to configure automatic deploy to Heroku
4. Deploy to staging and production envs
5. Write feature flag for applying dark mode styling
6. View logs for deployed app
