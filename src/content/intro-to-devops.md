---
title: Intro to DevOps
date: 2019-12-19
path: /intro-to-devops
---

## Resources

Here are links to resources mentioned during the workshop.

### Articles

* Annual State of DevOps reports
  * [2019 State of DevOps Report][Puppet DevOps Report] by Puppet
  * [2019 Accelerate State of DevOps Report][Google DevOps Report] by Google
* [Trunk Based Development][TBD]
* [Feature Toggles (aka Feature Flags)][Feature Toggles] by Martin Fowler

### Books

* [_The DevOps Handbook_][DevOps Handbook] by Gene Kim, Jez Humble, John Willis, Patrick Debois
* [_Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations_][Accelerate] by Nicole Forsgren, Jez Humble, Gene Kim
* [_The Art of Monitoring_][Art of Monitoring] by James Turnbull
* [_Site Reliability Engineering: How Google Runs Production Systems_][SRE] by Betsy Beyer, Chris Jones, Jennifer Petoff, Niall Richard Murphy
* [_The Phoenix Project: A Novel About IT, DevOps, and Helping Your Business Win_][Phoenix Project] by Kevin Behr, George Spafford, Gene Kim
* [_The Unicorn Project: A Novel About Developers, Digital Disruption, Thriving in the Age of Data_][Unicorn Project] by Gene Kim

### Other

* ["What is DevOps?" slides for Weirich][Weirich] by Eric Smith
* [DevOps learning trail][learning trail]
* [DevOpsDays][DevOpsDays] conferences around the world

[Puppet DevOps Report]: https://www.puppet.com/resources/report/state-of-devops-report/
[Google DevOps Report]: https://cloud.google.com/blog/products/devops-sre/the-2019-accelerate-state-of-devops-elite-performance-productivity-and-scaling
[DevOps Handbook]: https://itrevolution.com/book/the-devops-handbook/
[Accelerate]: https://itrevolution.com/book/accelerate/
[Weirich]: https://github.com/8thlight/weirich-curriculum/tree/master/slideshows/what-is-devops
[TBD]: https://trunkbaseddevelopment.com/
[Art of Monitoring]: https://artofmonitoring.com/
[Feature Toggles]: https://www.martinfowler.com/articles/feature-toggles.html
[SRE]: https://landing.google.com/sre/books/
[Phoenix Project]: https://itrevolution.com/book/the-phoenix-project
[Unicorn Project]: https://itrevolution.com/the-unicorn-project/
[DevOpsDays]: https://devopsdays.org/
[learning trail]: https://github.com/8thlight/learning-trails/blob/master/systems/devops.md

## Tools

DevOps is not just the tools...but there are a lot of tools used to enable DevOps. Here are some libraries, frameworks, applications, and services that you may end up using.

### Automated tests

* Test frameworks
* Test watchers

### Feature flags

* Language-specific libraries
* [LaunchDarkly][LaunchDarkly]

[LaunchDarkly]: https://docs.launchdarkly.com/docs

### Configuration as code

* [Ansible][Ansible]
* [Puppet][Puppet]
* [Chef][Chef]

[Ansible]: https://docs.ansible.com/ansible/latest/index.html
[Puppet]: https://www.puppet.com/docs/puppet/latest/puppet_index.html
[Chef]: https://docs.chef.io/


### Infrastructure as code

* [Terraform][Terraform]
* [AWS CloudFormation][CloudFormation]
* [Azure Resource Manager][Resource Manager]

[Terraform]: https://www.terraform.io/docs/index.html
[CloudFormation]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html
[Resource Manager]: https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-overview

### CI/CD

* [Travis CI][Travis]
* [Circle CI][Circle]
* [Jenkins][Jenkins]
* [Gitlab CI/CD][Gitlab]
* [Github Actions][Github Actions]
* [AWS CodePipeline][CodePipeline]
* [Azure Pipelines][Azure Pipelines]

[Travis]: https://docs.travis-ci.com/
[Circle]: https://circleci.com/docs/
[Jenkins]: https://jenkins.io/doc/
[Gitlab]: https://docs.gitlab.com/ee/ci/
[Github Actions]: https://github.com/features/actions
[CodePipeline]: https://docs.aws.amazon.com/codepipeline/latest/userguide/welcome.html
[Azure Pipelines]: https://docs.microsoft.com/en-us/azure/devops/pipelines/get-started/what-is-azure-pipelines?view=azure-devops

### Containers

* [Docker][Docker]
* [rkt][rkt]
* [containerd][containerd]

[Docker]: https://docs.docker.com/
[rkt]: https://coreos.com/rkt/
[containerd]: https://containerd.io/

### Logging, monitoring, tracing, alerts

* [Elastic Stack][ELK] (also known as ELK stack)
* [DataDog][DataDog]
* [AWS CloudWatch][CloudWatch]
* [Azure Monitor][Monitor]
* [Splunk][Splunk]
* [Sentry][Sentry]

[ELK]: https://www.elastic.co/guide/index.html
[DataDog]: https://docs.datadoghq.com/
[CloudWatch]: https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html
[Monitor]: https://docs.microsoft.com/en-us/azure/azure-monitor/
[Splunk]: https://docs.splunk.com/Documentation
[Sentry]: https://docs.sentry.io/
