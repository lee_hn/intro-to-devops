import React from "react"

const renderMarkdown = (data) => {
  const { frontmatter, tableOfContents, html } = data.markdownRemark;
  return (
    <div className="blog-post">
      <h1>{frontmatter.title}</h1>
      <h4 className="date">{frontmatter.date}</h4>
      <h2>Contents</h2>
      <div
        className="blog-post-toc"
        dangerouslySetInnerHTML={{ __html: tableOfContents }}
      />
      <div
        className="blog-post-content"
        dangerouslySetInnerHTML={{ __html: html }}
      />
    </div>
  )
}

export default renderMarkdown
